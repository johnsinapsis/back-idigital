import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ProductPersistence } from "./persistence/product.persistence";
import { RequestProduct } from "../application/requestProduct";
import { ValidateFieldsRequestProduct } from "../application/validations/validateFieldsRequestProduct";
import { ErrorRequestProduct } from "./handler/error.request.product";
import { ExecRulesRequestProduct } from "./rules/exec.rules.requestProduct";

export class RequestProductPostController extends Controller implements configControllerIdentity{

    private validateFields
    private rules
    private daoProduct
    private requestProduct

    constructor(){
        super()
        this.validateFields = new ValidateFieldsRequestProduct()
        this.rules = new ExecRulesRequestProduct()
        this.daoProduct = new ProductPersistence()
    }

    getConfigId(){
        return 'requestProduct'
    }

    async postRequestProduct(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                let {userId,type,requestNumber} = req.body
                let params = {userId,type,accountNumber:requestNumber,state:'Pending'}
                await this.rules.exec(params)
                let product = await this.daoProduct.createProduct(params)
                this.requestProduct = new RequestProduct(product)
                return res.json(this.requestProduct.map2Response())
            }
            else{
                let error = new ErrorRequestProduct()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}