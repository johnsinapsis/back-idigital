import { ProductTypePersistence } from "../persistence/productType.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { BusinessProductType } from "../../../shared/application/business.rules";

export class ExecRulesProductType implements BusinessProductType{
    private type = "ExecRulesProductType"
    private daoProductType

    constructor(){
        this.daoProductType = new ProductTypePersistence()
    }

    public async existProductType(productTypeId:number): Promise <boolean>{
        let product = await this.daoProductType.getProductType(productTypeId)
        if(!product.id)
            return false
        return true
    }

    public async exec(productId:number){
        if(! await this.existProductType(productId))
            this.throwBusiness("El tipo de producto no existe")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}