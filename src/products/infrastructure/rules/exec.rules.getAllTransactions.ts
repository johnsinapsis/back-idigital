import { BusinessError } from "../../../shared/application/handler/business.error";
import { ExecRulesUser } from "../../../users/infrastructure/rules/exec.rules.user";
import { ExecRulesProduct } from "./exec.rules.product";

export class ExecRulesGetAllTransactionByProduct{
    
    private type = "ExecRulesGetAllTransactionByProduct"
    private rulesUser
    private rulesProduct

    constructor(){
        this.rulesUser = new ExecRulesUser()
        this.rulesProduct = new ExecRulesProduct()
    }

    public async exec(req:any){
        await this.rulesUser.exec(req.userId)
        await this.rulesProduct.exec(req.productId)
        
    }

}