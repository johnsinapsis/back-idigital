import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ValidateFieldsAllProductsByUser } from "../application/validations/validateFieldsAllProducts";
import { ErrorGetAllProductsByUser } from "./handler/error.get.all.products";
import { ExecRulesGetAllProductsByUser } from "./rules/exec.rules.getAllProductsByuser";
import { ProductPersistence } from "./persistence/product.persistence";
import { GetAllProductsByUser } from "../application/getAllProductsByUser";
import { Authentication } from "../../shared/infrastructure/auhentication";

export class ShowAllProductsByUserGetController extends Controller implements configControllerIdentity{
    
    private validateFields
    private daoProducts
    private rules
    private getProductsByUser
    private auth

    constructor(){
        super()
        this.validateFields = new ValidateFieldsAllProductsByUser()
        this.rules = new ExecRulesGetAllProductsByUser()
        this.daoProducts = new ProductPersistence()
        this.auth = new Authentication()
    }

    getConfigId(){
        return 'products'
    }

    async getAllProductsByUser(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                await this.rules.exec(req.params.userId)
                let decoded = this.auth.validateToken(req.get('Authorization'),res);
                console.log("linea 35",decoded);
                let products = await this.daoProducts.getProductsByUser(req.params.userId)
                this.getProductsByUser = new GetAllProductsByUser(products)
                return res.json(this.getProductsByUser.map2Response())
            }
            else{
                let error = new ErrorGetAllProductsByUser()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}