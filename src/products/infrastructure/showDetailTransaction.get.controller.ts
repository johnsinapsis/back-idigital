import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { ValidateFieldsTransaction } from "../application/validations/validateFieldsTransaction"; 
import { ErrorGetDetailTransaction } from "./handler/error.get.detail.transaction";
import { ExecRulesGetTransaction } from "./rules/exec.rules.getTransaction";
import { TransactionPersistence } from "./persistence/transaction.persistence";
import { GetDetailTransaction } from "../application/getDetailTransaction";

export class ShowDetailTransactionGetController extends Controller implements configControllerIdentity{

    private validateFields
    private rules
    private daoTransactions
    private getTransaction

    constructor(){
        super()
        this.validateFields = new ValidateFieldsTransaction()
        this.rules = new ExecRulesGetTransaction()
        this.daoTransactions = new TransactionPersistence()
    }

    getConfigId(){
        return 'transaction'
    }

    async getDetailTransaction(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                await this.rules.exec(req.params.transactionId)
                let transaction = await this.daoTransactions.getTransaction(req.params.transactionId)
                this.getTransaction = new GetDetailTransaction(transaction)
                return res.json(this.getTransaction.map2Response())
            }
            else{
                let error = new ErrorGetDetailTransaction()
                return res.json(error.buildResponse(res))
            }
        }catch(e){
            console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }
}