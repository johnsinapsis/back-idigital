import { BaseErrorController } from "../../../controllers/base.error.controller";

export class ErrorGetAllProductsByUser extends BaseErrorController{

    constructor(){
        super()
        this.type = "ErrorGetAllProductsByUserFormat"
    }

}