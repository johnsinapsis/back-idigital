import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { ProductTypeRepository } from "../../application/repositories/productType.repository";
import { ProductType } from "../../domain/productType";
import { QueryTypes } from "sequelize"

export class ProductTypePersistence implements ProductTypeRepository{
    async getProductType(typeId:number): Promise <ProductType>{
        const productsType: any = await sequelize.query("SELECT * FROM product_types where id = ?", 
            { 
                replacements: [typeId],
                type: QueryTypes.SELECT 
            });
        return this.map2response(productsType)
    }

    private map2response(productsType:any): ProductType{
        if(productsType.length===0)
            return new ProductType
        let productType:ProductType = {
            id: productsType[0].id,
            name: productsType[0].name,
        }
        return productType
    }
}