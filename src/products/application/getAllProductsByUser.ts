import { UseCase } from "../../shared/application/base.useCase";
import { CommonResponse } from "../../shared/infrastructure/common.response";



export class GetAllProductsByUser implements UseCase{
    private products: Array<Object>
    type = "GetAllProductsByUser"
    constructor(products:Array<Object>){
        this.products = products
    }
    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Consulta realizada correctamente"
        res.response["products"] = this.products
        return res
    }
}