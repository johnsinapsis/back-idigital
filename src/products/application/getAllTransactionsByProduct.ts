import { UseCase } from "../../shared/application/base.useCase";
import { CommonResponse } from "../../shared/infrastructure/common.response";

export class GetAllTransactionsByProduct implements UseCase{
    private transactions: Array<Object>
    type = "GetAllTransactionsByProduct"
    constructor(transactions:Array<Object>){
        this.transactions = transactions
    }
    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Consulta realizada correctamente"
        res.response["transactions"] = this.transactions
        return res
    }
}

