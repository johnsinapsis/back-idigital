import { Product } from "../../domain/product";

export interface ProductRepository{
    getProductsByUser(userId:number):Promise <Array <Object>>
    getProduct(productId:number):Promise <Product>
    getProductByAccount(accountNumber:string):Promise <Product>
    createProduct(req:any):Promise <Product>
}