import { ProductType } from "../../domain/productType";

export interface ProductTypeRepository{
    getProductType(typeId:number):Promise <ProductType>
}