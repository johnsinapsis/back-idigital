import { Product } from "../domain/product";
import { CommonResponse } from "../../shared/infrastructure/common.response";
import { UseCase } from "../../shared/application/base.useCase";

export class RequestProduct implements UseCase{
    private product:Product
    type = "RequestProduct"
    constructor(product:Product){
        this.product = product
    }


    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Se ha creado una nueva solicitud de producto"
        res.response["product"] = this.product
        return res
    }

}