import { UseCase } from "../../shared/application/base.useCase";
import { CommonResponse } from "../../shared/infrastructure/common.response";
import { Transaction } from "sequelize/types";

export class GetDetailTransaction implements UseCase{
    private transaction:Transaction
    type = "GetDetailTransaction"
    constructor(transaction:Transaction){
        this.transaction = transaction
    }
    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Consulta realizada correctamente"
        res.response["transaction"] = this.transaction
        return res
    }
}