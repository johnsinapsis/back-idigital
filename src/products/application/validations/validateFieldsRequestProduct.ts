import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsRequestProduct extends BaseValidation{
    constructor(){
        super()
        let emptyUser = this.fieldValidationFactory.createInstance('empty','$.userId','El id del usuario es requerido')
        let emptyType = this.fieldValidationFactory.createInstance('empty','$.type','El tipo de producto es requerido')
        let emptyRequestNumber = this.fieldValidationFactory.createInstance('empty','$.requestNumber','El numero de solicitud es requerido')
        let numberUserId = this.fieldValidationFactory.createInstance('number', '$.userId', 'El id del usuario debe ser numérico');
        let numberType = this.fieldValidationFactory.createInstance('number', '$.type', 'El id del tipo de producto debe ser numérico');
        this.addValidator(emptyUser)
        this.addValidator(emptyType)
        this.addValidator(emptyRequestNumber)
        this.addValidator(numberUserId)
        this.addValidator(numberType)
    }
}