import express, {Router, urlencoded, json} from 'express'
import config from '../config/config.js'
import { RouteManager } from '../route/route.js'

export class ExpressServer{
    app = express();
    router = Router();
    routeManager = new RouteManager(this.router)

    constructor(){
        this.app.use(urlencoded({extended: false}))
        this.app.use(express.json())
        this.app.use((req,res,next)=>{
            let allowedOrigins = ['http://192.168.1.6:3000', 'http://localhost:3000'];
            let Host = req.get('Origin');
            //console.log(Host);
            if(Host){
                allowedOrigins.forEach(function(val, key){
                    //console.log("linea 19",Host);
                    if (Host.indexOf(val) > -1){
                        console.log("linea 20");
                        res.setHeader('Access-Control-Allow-Origin', Host);
                    }
                })
            }
            
            //res.setHeader('Access-Control-Allow-Origin','http://192.168.1.6:3000')
            res.setHeader('Access-Control-Allow-Headers','X-Requested-With,content-type')
            next()
        })
        this.app.use(this.router)
    }

    startup(){
        const {port} = config
        this.app.listen(port, function() {
            console.log('Servidor web escuchando en el puerto '+ port)
        });
    }

    getApp(){
        return this.app
    }

    getRouter(){
        return this.router
    }

    getRouteManager(){
        return this.routeManager       
    }

}