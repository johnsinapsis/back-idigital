import { RequestDto } from "../dtos/request.dto";
import { Request } from "../../domain/request";
import { Mapper } from "../../../shared/application/mapper";

export class RequestMapper extends Mapper{
    
    public map2RequestDto(request:Request){
        let requestDto = new RequestDto();
        requestDto.setMethod(request.method)
        requestDto.setId(request.id)
        requestDto.setData(request.data)
        requestDto.setDate(request.date)
        return requestDto
    }

    
    public map2Entity(req:any){
        let request = new Request();
        request.data = req.data
        request.date = req.date
        request.method = req.method
        return request
    }
    
    
}