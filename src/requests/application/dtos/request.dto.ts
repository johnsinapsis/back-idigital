export class RequestDto{
    private id:number
    private date:String
    private method:string
    private data:string

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public getDate(): String {
        return this.date;
    }

    public setDate(date: String): void {
        this.date = date;
    }

    public getMethod(): string {
        return this.method;
    }

    public setMethod(method: string): void {
        this.method = method;
    }

    public getData(): string {
        return this.data;
    }

    public setData(data: string): void {
        this.data = data;
    }

}