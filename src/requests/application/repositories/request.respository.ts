import { Request } from "../../domain/request";
import { RequestDto } from "../dtos/request.dto";

export interface RequestRepository{
    createRequest(request:Request): Promise <RequestDto>
}