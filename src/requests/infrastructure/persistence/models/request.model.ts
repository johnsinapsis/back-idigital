import { Sequelize, DataTypes, Model } from 'sequelize'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class RequestModel{

    public entity

    constructor(){
        this.entity = sequelize.define('request',{
            id: {
                type: DataTypes.INTEGER,
                autoIncrement:true,
                allowNull: false,
                primaryKey: true
            },
            method: {
                type: DataTypes.STRING,
                field: 'method',
            },
            data: {
                type: DataTypes.TEXT,
                field: 'data',
            },
            date: {
                type: DataTypes.DATE,
                field: 'date',
            }
        },
        {
            timestamps: false
        })
    }
}