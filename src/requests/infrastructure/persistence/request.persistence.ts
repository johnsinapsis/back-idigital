import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { QueryTypes } from "sequelize"
import { Request } from "../../domain/request";
import { RequestRepository } from "../../application/repositories/request.respository";
import { RequestDto } from "../../application/dtos/request.dto";
import { RequestModel } from "./models/request.model";
import { RequestMapper } from "../../application/mappers/request.mapper";

export class RequestPersistence implements RequestRepository{
    
    private mapper

    constructor() {
        this.mapper = new RequestMapper()
    }
    
    public async createRequest(request:Request): Promise <RequestDto>{
        let requestModel = new RequestModel()
        const newRequestModel = await requestModel.entity.create({
            method: request.method,
            data: request.data,
            date: request.date
        })
        let requestDto = this.mapper.map2RequestDto(newRequestModel.dataValues)
        return requestDto
    }
}