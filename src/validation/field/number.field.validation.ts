import { FieldValidator } from '../field/field.validation'
import jp from 'jsonpath'
import { Utils } from '../../shared/application/utilities'
import { INV_FIELDS, UNDEF } from '../../config/constants'

export class NumberFieldValidation extends FieldValidator{
    
    constructor(jpExpr, failMsg){
        super(jpExpr, failMsg)
        this.jpExpr = jpExpr
        this.failMsg = failMsg
    }


    isValid(req, res){
        let field:string = this.jpExpr.split('.')[1]
        let prop = req.body
        if(req.params){
            if(req.params.hasOwnProperty(field))
                prop = req.params
        }
        field = jp.query(prop, this.jpExpr)[0]
        if(!field)
            return true
        return Utils.isNumber( field )
    }

    addErrorResponse( res, msg ){        
        if( typeof res[INV_FIELDS] === UNDEF ){
            res.invalid_fields = []
        }
        res.invalid_fields.push(msg)
    }

}