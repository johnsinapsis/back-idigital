import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class AllProductsByUserUseCase extends BaseUseCase{

    private route = "/products"
    private userId
    constructor(){
        super()
    }

    init(userId){
        this.userId = userId
    }

    async test(){
        let route = this.url+this.route+'/'+this.userId
        let res = await axios.get(route)
        return res.data
    }
}