import config, { ConfigReader } from '../../config/config'

export class BaseUseCase{
    protected cr 
    protected url
    //private config
    constructor(){
        this.cr = ConfigReader
        this.url = 'http://localhost:'+config.port
    }

    protected getRandomInt(min:number,max:number){
        min = Math.ceil(min);
        max = Math.ceil(max);
        return Math.floor(Math.random() * (max - min) + min)
    }
}

