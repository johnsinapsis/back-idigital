import axios from 'axios'
import { BaseUseCase } from './base.useCase'

export class AllTransactionsByProductUseCase extends BaseUseCase{

    private route = "/products/transactions"
    private request = {}
    constructor(){
        super()
    }

    init(userId,productId){
        this.request = {userId,productId}
    }

    async test(){
        let route = this.url+this.route
        let res = await axios.post(route,this.request)
        return res.data
    }
}