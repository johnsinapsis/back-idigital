import { assert } from 'chai'
import { AllProductsByUserUseCase } from './useCases/all.products.by.user'

let allProductsByUser = new AllProductsByUserUseCase()
let userId:number = 1
let userIdFail:string = 'abc'
let userIdSessionEnded:number = 3
let userIdNotExist: number = 99
let msg:string = ""
let result = null

describe("Obtener todos los productos relacionados con el usuario",()=>{
    describe("Probando que el id de usuario valide es numérico", ()=>{
        it("La validación debe devolver mensaje de error: El id del usuario debe ser numérico", async()=>{
            allProductsByUser.init(userIdFail)
            result = await allProductsByUser.test()
            if(result)
                assert.equal(result['response']['error'],"El id del usuario debe ser numérico")
        })
    })
    describe("Probando las validaciones de negocio",()=>{
        it("La validación debe devolver mensaje de error: El usuario no existe o no es de tipo cliente", async()=>{
            allProductsByUser.init(userIdNotExist)
            result = await allProductsByUser.test()
            if(result)
                assert.equal(result['response']['error'],"El usuario no existe o no es de tipo cliente")
        })
        it("La validación debe devolver mensaje de error: El usuario no tiene una sesión activa", async()=>{
            allProductsByUser.init(userIdSessionEnded)
            result = await allProductsByUser.test()
            if(result)
                assert.equal(result['response']['error'],"El usuario no tiene una sesión activa")
        })
        it("Devuelve el mensaje: Consulta realizada correctamente", async()=>{
            allProductsByUser.init(userId)
            result = await allProductsByUser.test()
            if(result)
                assert.equal(result['response']['description'],"Consulta realizada correctamente")
        })
    })
})