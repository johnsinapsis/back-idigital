import { assert } from 'chai'
import { AverageTransactionsUseCase } from './useCases/average.transactions'

let averageTransactions = new AverageTransactionsUseCase()
let userId:number = 1
let productIdFail:string = 'abc'
let productId = 1
let productNotExist = 99
let dateStart = '2021-08-23'
let dateEnd = '2021-08-25'
let result = null

describe("Promediar las transacciones por producto",()=>{
    describe("Probando que el id de producto valide es numérico", ()=>{
        it("La validación debe devolver mensaje de error: El id del producto debe ser numérico", async()=>{
            averageTransactions.init({userId,productId:productIdFail,dateStart,dateEnd})
            result = await averageTransactions.test()
            if(result)
                assert.equal(result['response']['error'],"El id del producto debe ser numérico")
        })
        it("La validación debe devolver mensaje de error: La fecha final debe ser mayor a la inicial", async()=>{
            averageTransactions.init({userId,productId,dateStart:dateEnd,dateEnd:dateStart})
            result = await averageTransactions.test()
            if(result)
                assert.equal(result['response']['error'],"La fecha final debe ser mayor a la inicial")
        })
    })
    describe("Probando las validaciones de negocio",()=>{
        it("La validación debe devolver mensaje de error: El producto no existe", async()=>{
            averageTransactions.init({userId,productId:productNotExist,dateStart,dateEnd})
            result = await averageTransactions.test()
            if(result)
                assert.equal(result['response']['error'],"El producto no existe")
        })
        it("Devuelve el mensaje: Consulta realizada correctamente", async()=>{
            averageTransactions.init({userId,productId,dateStart,dateEnd})
            result = await averageTransactions.test()
            if(result)
                assert.equal(result['response']['description'],"Consulta realizada correctamente")
        })
    })
})