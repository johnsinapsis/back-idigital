import { assert } from 'chai'
import { LoginUserClientUseCase } from './useCases/login.user.client'

let loginUser = new LoginUserClientUseCase()
let documentNumberSuccess:string = '123456'
let documentNumberFail:string = '999999'
let password:string = '123456'
let msg:string = ""
let result = null

describe("Probar login de usuario",()=>{
    describe("Probando la validacion de los campos requeridos para el login", ()=>{
        it("La validación debe devolver mensaje de error: el documento es requerido", async()=>{
            loginUser.init("",password)
            result = await loginUser.test()
            if(result){
                assert.equal(result['response']['error'],"El documento es requerido")
            }
        })
    })
    describe("Probando la validación de credenciales",()=>{
        it("Debe devolver mensaje de error: Las credenciales son inválidas", async()=>{
            loginUser.init(documentNumberFail,password)
            result = await loginUser.test()
            if(result)
                assert.equal(result['response']['error'],"Las credenciales son inválidas")
        })
        it("Devuelve el mensaje: Se ha creado una nueva sesión correctamente", async()=>{
            loginUser.init(documentNumberSuccess,password)
            result = await loginUser.test()
            if(result)
                assert.equal(result['response']['description'],"Se ha creado una nueva sesión correctamente")
        })
    })
})



