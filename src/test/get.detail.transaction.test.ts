import { assert } from 'chai'
import { GetDetailTransactionUseCase } from "./useCases/get.detail.transaction";

let getDetailTransaction = new GetDetailTransactionUseCase
let transactionId = 1
let transactionIdFail:string = 'abc'
let transactionIdNotExist: number = 99
let result = null

describe("Obtener el detalle de una transacción",()=>{
    describe("Probando que el id de transacción valide es numérico", ()=>{
        it("La validación debe devolver mensaje de error: El id de la transacción debe ser numérico", async()=>{
            getDetailTransaction.init(transactionIdFail)
            result = await getDetailTransaction.test()
            if(result)
                assert.equal(result['response']['error'],"El id de la transacción debe ser numérico")
        })
    })
    describe("Probando las validaciones de negocio",()=>{
        it("La validación debe devolver mensaje de error: La transacción no existe", async()=>{
            getDetailTransaction.init(transactionIdNotExist)
            result = await getDetailTransaction.test()
            if(result)
                assert.equal(result['response']['error'],"La transacción no existe")
        })
        it("Devuelve el mensaje: Consulta realizada correctamente", async()=>{
            getDetailTransaction.init(transactionId)
            result = await getDetailTransaction.test()
            if(result)
                assert.equal(result['response']['description'],"Consulta realizada correctamente")
        })
    })
})

