import { Sequelize, DataTypes, Model, Dialect } from 'sequelize'
import config from '../../config/config';

let {database,user, pass, host, port, motor} = config.db 

export const sequelize = new Sequelize(database, user, pass, {
    host,
    port: <number> port,
    dialect: <Dialect> motor 
});