import { BaseError } from "./base.error";

export class BusinessError extends BaseError{
    constructor(public type="",public response){
        super(type,response)  
    }

}