import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { PostPersistence } from "./persistence/post.persistence"

export class getAllPostsController extends Controller implements configControllerIdentity{

    private daoPosts

    constructor(){
        super()
        this.daoPosts = new PostPersistence()
    }

    getConfigId(){
        return 'posts'
    }

    async getPosts(req,res){
        try{
            let posts = await this.daoPosts.getPosts();
            return res.json(posts)
        }
        catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}