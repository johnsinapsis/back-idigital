import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { PostRepository } from "../../application/repositories/post.repository";
import { Post } from "../../domain/post";
import { QueryTypes } from "sequelize"

export class PostPersistence implements PostRepository{

    public async getPosts(): Promise <Array <Post>>{
        let sql = "SELECT * from posts"
        const posts: any = await sequelize.query(sql,{type: QueryTypes.SELECT })
        let arrayPost = []
        for(let i=0; i<posts.length; i++){
            let row:Post = this.map2response(posts,i)
            arrayPost.push(row)
        }
        return arrayPost
    }

    private map2response(posts:any, pos): Post{
        if(posts.length===0)
            return new Post
        let post:Post = {
            id: posts[pos].id,
            userId: posts[pos].user_id,
            title: posts[pos].title,
            body: posts[pos].body,
            picture: posts[pos].picture
        }
        return post
    }
}