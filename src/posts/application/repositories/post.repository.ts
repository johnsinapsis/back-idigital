import { Post } from "../../domain/post";

export interface PostRepository{
    getPosts():Promise<Array <Post>>
}