import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsRegisterUser extends BaseValidation{
    constructor(){
        super()
        let emptyLogin = this.fieldValidationFactory.createInstance('empty','$.email','El documento es requerido')
        let emptyPassword = this.fieldValidationFactory.createInstance('empty','$.password','El password es requerido')
        let emptyName = this.fieldValidationFactory.createInstance('empty','$.name','El nombre es requerido')
        this.addValidator(emptyLogin)
        this.addValidator(emptyPassword)
        this.addValidator(emptyName)
    }
}