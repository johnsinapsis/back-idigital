import { BaseValidation } from "../../../validation/base.validation";

export class ValidateFieldsGetPhotosByUser extends BaseValidation{
    constructor(){
        super()
        let numberUserId = this.fieldValidationFactory.createInstance('number', '$.userId', 'El id del usuario debe ser numérico');
        this.addValidator(numberUserId)
    }
}