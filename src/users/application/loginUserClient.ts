import { Session } from "../domain/session";
import { CommonResponse } from "../../shared/infrastructure/common.response";
import { UseCase } from "../../shared/application/base.useCase";

export interface BussinesRuleLogin{
    areCredentialsSuccess(req:Object): Promise <boolean>
}

export class LoginUserClient implements UseCase{
    private session:Session
    type = "LoginUserClient"
    constructor(session:Session){
        this.session = session
    }


    map2Response(){
        let res = new CommonResponse()
        res.type = this.type
        res.response.description = "Se ha creado una nueva sesión correctamente"
        res.response["session"] = this.session
        return res
    }

}