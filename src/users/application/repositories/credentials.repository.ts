import { Session } from "../../domain/session";

export interface UserCredentialsRepository{
    createSession(req: Object): Promise <Session>
    getSessionActiveByUserId(userId:number): Promise <Session>
}