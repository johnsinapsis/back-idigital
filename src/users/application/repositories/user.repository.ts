import { User } from "../../domain/user";
import { UserDto } from "../dtos/user.dto";
import { UserApi } from "../../domain/user.api";

export interface UserRepository{
    getUserById(userId:number): Promise <User>
    getUserByCredentials(req: Object): Promise <User>
    getUserByEmail(email: String): Promise <User>
    createUser(user:User): Promise <UserDto>
}

export interface UserApiRepository{
    getUsers():Promise <Array <UserApi>>
}