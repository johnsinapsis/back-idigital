import { UserPhoto } from "../../domain/userPhoto";


export interface UserPhotoRepository{
    getUserPhotos(userId:number):Promise <Array <UserPhoto>>
}