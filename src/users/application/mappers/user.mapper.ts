import { UserDto } from "../dtos/user.dto";
import { User } from "../../domain/user";
import { Utils } from "../../../shared/application/utilities";
import { Mapper } from "../../../shared/application/mapper";

export class UserMapper extends Mapper {
    public map2UserDto(user:User){
        let userDto = new UserDto();
        userDto.setEmail(user.email);
        userDto.setUserId(user.id);
        userDto.setName(user.name);
        return userDto
    }

    public map2Entity(req:any){
        let user = new User();
        user.email=req.email
        user.name = req.name
        user.password = Utils.encrypt(req.password)
        return user
    }
}