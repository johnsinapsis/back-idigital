export class UserPhoto{
    id: number
    albumId: number
    title: string
    url: string
    thumbnailUrl: string
}