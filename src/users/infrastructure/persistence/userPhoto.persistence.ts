import axios from 'axios'
import { UserPhotoRepository } from '../../application/repositories/userPhoto.repository'
import { UserPhoto } from '../../domain/userPhoto'

export class UserPhotoPersistence implements UserPhotoRepository{

    private url = "https://jsonplaceholder.typicode.com/users/"

    public async getUserPhotos(userId:number):Promise <Array <UserPhoto>>{
        let result = await axios.get(this.url+userId+'/photos')
        let arrayPhotos = []
        for(let i=0; i<result.data.length; i++){
            let row:UserPhoto = this.map2response(result.data,i)
            arrayPhotos.push(row)
        }
        return arrayPhotos
    }

    private map2response(userPhoto:any, pos): UserPhoto{
        if(userPhoto.length===0)
            return new UserPhoto
        let photo:UserPhoto = {
            id: userPhoto[pos].id,
            albumId: userPhoto[pos].albumId,
            title: userPhoto[pos].title,
            url: userPhoto[pos].url,
            thumbnailUrl: userPhoto[pos].thumbnailUrl,
        }
        return photo
    }

}