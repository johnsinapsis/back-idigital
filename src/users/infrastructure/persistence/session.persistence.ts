import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { UserCredentialsRepository } from "../../application/repositories/credentials.repository";
import { UserPersistence } from "./user.persistence";
import { Session } from "../../domain/session";
import { QueryTypes } from "sequelize"
import { Utils } from "../../../shared/application/utilities";
import { SessionModel } from "./models/session.model";

export class SessionPersistence implements UserCredentialsRepository{
    private userPersistence

    constructor(){
        this.userPersistence = new UserPersistence()
    }
    public async createSession(req): Promise <Session>{
        let user = await this.userPersistence.getUserByEmail(req.email)
        let sessionId = Utils.uuidv4()
        let sessionModel = new SessionModel()       
        let currentDate = Utils.setLocalDate(new Date())
        const newSession = await sessionModel.session.create({
            id: sessionId,
            userId: user.id,
            dateStart: currentDate,
            dateEnd: null
        })
        
        let session:Session = {
            id: sessionId,
            userId: user.id,
            dateStart: currentDate,
            dateEnd: null
        }
        return session
    }

    public async getSessionActiveByUserId(userId):Promise <Session>{
        const sessions: any = await sequelize.query("SELECT * FROM `sessions` where user_id = ? AND date_end is NULL", 
            { 
                replacements: [userId],
                type: QueryTypes.SELECT 
            });
        if(sessions.length===0)
            return new Session
        let session:Session ={
            id: sessions[0].id,
            userId,
            dateStart: sessions[0].dateStart,
            dateEnd: sessions[0].dateEnd
        }
        return session
    }
}