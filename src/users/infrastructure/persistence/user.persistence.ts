import { sequelize } from "../../../shared/infrastructure/base.persistence";
import { UserRepository } from "../../application/repositories/user.repository";
import { User } from "../../domain/user"
import { QueryTypes } from "sequelize"
import { UserModel } from "./models/user.model";
import { UserMapper } from "../../application/mappers/user.mapper";
import { UserDto } from "../../application/dtos/user.dto";


export class UserPersistence implements UserRepository{

    public async getUserByCredentials(req): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` where email = ? AND password = ?", 
            { 
                replacements: [req.email, req.password],
                type: QueryTypes.SELECT 
            });
        
        return this.map2response(users)
    }

    public async getUserById(userId:number): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` id = ? ", 
            { 
                replacements: [userId],
                type: QueryTypes.SELECT 
            });
        return this.map2response(users)
    }

    public async getUserByEmail(email:string): Promise <User>{
        const users: any = await sequelize.query("SELECT * FROM `users` WHERE email = ?",{
            replacements:[email],
            type: QueryTypes.SELECT
        })
        return this.map2response(users)
    }

    public async createUser(user:User): Promise <UserDto>{
        let userModel = new UserModel()
        const newUser = await userModel.user.create({
            email: user.email,
            name: user.name,
            password: user.password,
        });
        let userRegistered:User = newUser.dataValues
        let mapper = new UserMapper();
        let userDto = mapper.map2UserDto(userRegistered)
        return userDto
    }

    private map2response(users:any): User{
        if(users.length===0)
            return new User
        let user:User = {
            id: users[0].id,
            email: users[0].email,
            name: users[0].name,
            password: users[0].password
        }
        return user
    }

    
}