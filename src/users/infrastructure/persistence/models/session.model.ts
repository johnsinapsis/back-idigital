import { Sequelize, DataTypes, Model } from 'sequelize'
import { sequelize } from '../../../../shared/infrastructure/base.persistence'

export class SessionModel{
    public session

    constructor(){
        this.session = sequelize.define('session',{
            id: {
                type: DataTypes.STRING,
                allowNull: false,
                primaryKey: true
              },
            userId:{
                type: DataTypes.NUMBER,
                field: 'user_id'
            },
            dateStart: {
                type: DataTypes.DATE,
                field: 'date_start',
            },
            dateEnd: {
                type: DataTypes.DATE,
                field: 'date_end',
            },
        },
        {
            timestamps: false
        }
        )
    }
}
