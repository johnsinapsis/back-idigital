import axios from 'axios'
import { UserApiRepository } from "../../application/repositories/user.repository";
import { UserApi } from "../../domain/user.api";

export class UserApiPersistence implements UserApiRepository{
    private url = "https://jsonplaceholder.typicode.com/users/"

    public async getUsers(): Promise <Array <UserApi>>{
        let result = await axios.get(this.url)
        //let userApi = new UserApi()
        let arrayuser = []
        for(let i=0; i<result.data.length; i++){
            let row:UserApi = this.map2response(result.data,i)
            arrayuser.push(row)
        }
        return arrayuser
    }

    private map2response(users:any, pos): UserApi{
        if(users.length===0)
            return new UserApi
        let user:UserApi = {
            id: users[pos].id,
            name: users[pos].name,
            username: users[pos].username,
            email: users[pos].email,
            phone: users[pos].phone,
            website: users[pos].website,
            company: users[pos].company,
            address: users[pos].address,
        }
        return user
    }
}