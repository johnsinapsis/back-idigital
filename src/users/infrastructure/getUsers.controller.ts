import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { UserApiPersistence } from "./persistence/user.api.persistence";
import { RequestDto } from "../../requests/application/dtos/request.dto";
import { CreateRequest } from "./createRequest";


export class GetUsersController extends Controller implements configControllerIdentity{

    private daoUsers
    private requestDto

    constructor(){
        super()
        this.daoUsers = new UserApiPersistence()
        this.requestDto = new RequestDto()
    }

    getConfigId(){
        return 'users'
    }

    async getUsers(req,res){
        try{
            let users = await this.daoUsers.getUsers()
            if(users){
                this.requestDto.setMethod('getUsers')
                let formatUser = JSON.stringify(users)
                this.requestDto.setData(formatUser)
               let createRequest = new CreateRequest(this.requestDto)
               await createRequest.saveRequest()
            }
            return res.json(users)
        }
        catch(e){
            console.log(e);
            return BaseErrorHandler.handle(res,e)
        }
    }

}
