import { RequestPersistence } from "../../requests/infrastructure/persistence/request.persistence";
import { RequestDto } from "../../requests/application/dtos/request.dto";
import { Request } from "../../requests/domain/request";
import { RequestMapper } from "../../requests/application/mappers/request.mapper";

export class CreateRequest{

    private requestDto:RequestDto
    private daoRequest
    private mapper

    constructor(requestDto:RequestDto){
        this.requestDto = requestDto
        this.daoRequest = new RequestPersistence()
        this.mapper = new RequestMapper()
    }

    public async saveRequest(){
        let request:Request = this.mapper.map2Entity(this.requestDto)
        let newRequest = await this.daoRequest.createRequest(request)
        return newRequest ? true : false
    }

}