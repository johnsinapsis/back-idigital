import { BaseErrorController } from "../../../controllers/base.error.controller";

export class ErrorGetPhotosByUser extends BaseErrorController{

    constructor(){
        super()
        this.type = "ErrorGetPhotosByUser"
    }

    public toResponse(res,response){
        let stack = new Error().stack
        let newRes = this.map2response(response,res.invalid_fields[0],stack)
        return {
            type: this.type,
            response:newRes
        } 
    }
}