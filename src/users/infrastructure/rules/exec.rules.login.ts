import { BussinesRuleLogin } from "../../application/loginUserClient";
import { UserPersistence } from "../persistence/user.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { Utils } from "../../../shared/application/utilities";

export class ExecRulesLogin implements BussinesRuleLogin{

    private type = "ExecRulesLogin"
    private daoUser

    constructor(){
        this.daoUser = new UserPersistence()
    }

    async areCredentialsSuccess(req:Object){
        let user = await this.daoUser.getUserByEmail(req['email'])
        console.log(user);
        if(!user.id)
            return false
        //aca se debe validar si el password es el correcto
        return Utils.compareEncrypt(req['password'],user.password)
    }

    public async exec(req:Object,res){
        if(! await this.areCredentialsSuccess(req))
            this.throwBusiness("Las credenciales son inválidas")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}