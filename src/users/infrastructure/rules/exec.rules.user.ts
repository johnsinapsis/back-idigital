import { UserPersistence } from "../persistence/user.persistence"; 
import { SessionPersistence } from "../persistence/session.persistence";
import { BusinessError } from "../../../shared/application/handler/business.error";
import { BusinessSession, BusinessUser } from "../../../shared/application/business.rules";

export class ExecRulesUser implements BusinessUser,BusinessSession{
    private type = "ExecRulesUser"
    private daoUser
    private daoSession

    constructor(){
        this.daoUser = new UserPersistence()
        this.daoSession = new SessionPersistence()
    }

    async existUser(userId){
        let user = await this.daoUser.getUserById(userId)
        if(!user.id)
            return false
        return true
    }

    async isSessionActiveUser(userId){
        let session = await this.daoSession.getSessionActiveByUserId(userId)
        if(!session.id)
            return false
        return true
    }

    public async exec(userId:number){
        if(! await this.existUser(userId))
            this.throwBusiness("El usuario no existe o no es de tipo cliente")
        if(! await this.isSessionActiveUser(userId))
            this.throwBusiness("El usuario no tiene una sesión activa")
    }

    public throwBusiness(msg){
        let response = {error: msg}
        throw new BusinessError(this.type,response)
    }
}