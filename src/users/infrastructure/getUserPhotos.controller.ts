import { Controller, configControllerIdentity } from "../../controllers/controller.base";
import { BaseErrorHandler } from "../../shared/infrastructure/base.error.handler";
import { UserPhotoPersistence } from "./persistence/userPhoto.persistence";
import { ValidateFieldsGetPhotosByUser } from "../application/validations/validateFieldsGetPhotosByUser";
import { ErrorGetPhotosByUser } from "./handler/error.getPhotosbyUser"; 

export class GetUserPhotosController extends Controller implements configControllerIdentity{

    private validateFields
    private daoUserPhoto

    constructor(){
        super()
        this.validateFields = new ValidateFieldsGetPhotosByUser()
        this.daoUserPhoto = new UserPhotoPersistence()
    }

    getConfigId(){
        return 'userPhotos'
    }

    async getUserPhotos(req,res){
        try{
            if(this.validateFields.isValid(req,res)){
                let userPhotos = await this.daoUserPhoto.getUserPhotos(req.param.userId)
                return res.json(userPhotos)

            }
            else{
                let error = new ErrorGetPhotosByUser
                return res.json(error.buildResponse(res))
            }
        }
        catch(e){
            return BaseErrorHandler.handle(res,e)
        }
    }
}